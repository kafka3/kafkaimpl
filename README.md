# Kafka Implementation
This project contains basic Kafka implementation using 2 consumers and 1 producer

## Initial setup

- Download and start zookeeper and kafka servers
- [Reference](https://kafka.apache.org/quickstart) 

## Run project
```
1. mvn clean install(make sure to start zookeeper and kafka servers)
2. java -jar kafka-0.0.1-SNAPSHOT.jar
```

## About project

- **KafkaProducerConfig**: Configuration file consists of Producer details
- **KafkaConsumerConfig**: Configuration file consists of Consumer details
- **KafkaTopicConfig**: In this project, 2 topics were used. This configuration file contains Kafka admin and topic details.


- **KafkaConsumers**: These services are for both topics. These services would listen to those topics and print the messages.
- **KafkaProducer**: Service class responsible to send messages to topics. Topics are picked up randomly.

To test the basic functionality, this project has a scheduler which runs every 20 seconds and push message to either Topic. 