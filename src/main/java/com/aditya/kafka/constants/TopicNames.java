package com.aditya.kafka.constants;

public class TopicNames {
    public static final String TOPIC_FIRST = "first_topic";
    public static final String TOPIC_SECOND = "second_topic";

}
