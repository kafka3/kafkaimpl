package com.aditya.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class KafkaImplementationApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaImplementationApplication.class, args);
	}

}
