package com.aditya.kafka.services;

import com.aditya.kafka.constants.TopicNames;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class KafkaFirstConsumerService {

    @KafkaListener(topics = TopicNames.TOPIC_FIRST, groupId = "${kafka.groupId}")
    public void listenMessage(String message){
        log.info("Message received on First Topic: "+message);
    }
}
