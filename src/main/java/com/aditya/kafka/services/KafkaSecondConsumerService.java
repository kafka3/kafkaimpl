package com.aditya.kafka.services;

import com.aditya.kafka.constants.TopicNames;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class KafkaSecondConsumerService {

    @KafkaListener(topics = TopicNames.TOPIC_SECOND, groupId = "${kafka.groupId}")
    public void listenTopic(String message){
        log.info("Message received on Second Topic: "+message);
    }
}
