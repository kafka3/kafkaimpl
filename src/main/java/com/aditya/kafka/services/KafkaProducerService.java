package com.aditya.kafka.services;

import com.aditya.kafka.constants.TopicNames;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalField;
import java.util.Date;

@Service
@Slf4j
public class KafkaProducerService {

    @Autowired
    private KafkaTemplate kafkaTemplate;

    @Scheduled(fixedDelay = 20000)
    public void produceMessage(){
        System.out.println("Trying to send message to kafka");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDate now = LocalDate.now();
        String date = formatter.format(now);
        sendMessage(getTopicName(), date, "Message sent at "+date);
    }

    private String getTopicName(){
        Double randomNum = Math.random()*10;
        return (randomNum.intValue() % 2 == 0)?TopicNames.TOPIC_FIRST:TopicNames.TOPIC_SECOND;
    }

    @Async
    private void sendMessage(String topic, String key, String message){
        ListenableFuture<SendResult<String, String>> future = kafkaTemplate.send(topic, key, message);
        future.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {
            @Override
            public void onFailure(Throwable throwable) {
                log.error("Message: "+message+" not sent due to: "+throwable.getLocalizedMessage());
            }

            @Override
            public void onSuccess(SendResult<String, String> stringStringSendResult) {
                log.info("Message: "+message+" sent");
            }
        });
    }
}
